﻿using BooksWebService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Client
{
    class Program
    {
        private static JavaScriptSerializer jsonSerializer;
        private const string SELLER_TYPE = "1";
        private const string BUYER_TYPE = "2";

        static void Main(string[] args)
        {
            jsonSerializer = new JavaScriptSerializer();

            bool executeAnotherOperation = true;

            


            do
            {
                DisplayUserOptions();
                string selection = Console.ReadLine();
                PerformUserOperation(selection);

            } while (executeAnotherOperation && !WantToFinish());

        }

        private static void PerformUserOperation(string selection)
        {
                switch (selection)
                {
                    case "1":
                        DisplayAllBooks();
                        break;
                    case "2":
                        DisplayBookWithTitle();
                        break;
                    case "3":
                        AddBook();
                        break;
                    case "4":
                        DeleteBook();
                        break;
                    case "5":
                        Update();
                        break;
                    case "6":
                        BuyBook();
                        break;
                    case "7":
                        break;
                }
        }

        private static void DisplayUserOptions()
        {
            Console.WriteLine("1. Wyświetl listę książek." +
                                      "\n2. Wyświetl dane książki." +
                                      "\n3. Dodaj książkę." +
                                      "\n4. Usuń książkę." +
                                      "\n5. Zmień dane książki." +
                                      "\n6. Kup książkę." +
                                      "\n7. Zakończ."
                                      );
        }


        private static bool WantToFinish()
        {
            Console.Write("Czy chcesz zakończyć? (T/N):");
            return Console.ReadLine().ToUpper().Equals("T");
        }

        private static void DisplayAllBooks()
        {
            string response = HttpService.getResponse(HttpService.GET_ALL_JSON_URL);
            List<Book> booksList = jsonSerializer.Deserialize<List<Book>>(response);
            if (booksList == null || booksList.Count == 0)
            {
                Console.WriteLine("Brak książek.");
                return;
            }
            for (int i = 0; i < booksList.Count; i++)
            {
                Book book = booksList[i];
                Console.WriteLine((i + 1) + ". Tytuł: " + book.Title +", cena: "+book.Price+", sztuk: " + book.NumberOfBooks);
            }
            Console.WriteLine();
        }

        private static void DisplayBookWithTitle()
        {
            string title = GetInput("Podaj tytuł książki: ");
            if(title== null)
            {
                return;
            }
            Book book = GetBookWithTitle(title);
            if (book == null)
            {
                return;
            }
            Console.WriteLine("Tytuł: " + book.Title + ", cena: " + book.Price + ", sztuk: " + book.NumberOfBooks);
        }

        private static Book GetBookWithTitle(string title)
        {
            if(title == "")
            {
                Console.WriteLine("Podaj tytuł.");
                return null;
            }
            Book book = jsonSerializer.Deserialize<Book>(HttpService.getResponse(HttpService.GET_ALL_JSON_URL + title));
            if (book == null)
            {
                Console.WriteLine("Nie ma książek o tytule: " + title);
                return null;
            }
            return book;
        }

        private static void AddBook()
        {
            string title = GetInput("Podaj tytuł książki: ");
            int price = GetIntInput("Podaj cenę książki: ");
            int numberOfBooks = GetIntInput("Podaj liczbę sztuk: ");
            Book book = new Book(title, price, numberOfBooks);
            Console.WriteLine(HttpService.putPostResponse(HttpService.GET_ALL_JSON_URL, "POST", jsonSerializer.Serialize(book)));
        }


        private static void DeleteBook()
        {
            string title = GetInput("Podaj tytuł książki: ");
            if (title == "")
            {
                Console.WriteLine("Nie podałeś tytułu.");
            }
            else {
                Console.WriteLine(HttpService.deleteResponse(title));
            }
        }

        private static void Update()
        {
            string title = GetInput("Podaj tytuł ksiazki: ");
            int price = GetIntInput("Podaj cenę książki: ");
            int numberOfBooks = GetIntInput("Podaj liczbę sztuk: ");
            if (title == "" || price == -1 || numberOfBooks == -1)
            {
                Console.WriteLine("Podaj poprawne dane.");
            }
            else {
                Book book = new Book(title, price, numberOfBooks);
                UpdateBook(book, title);
            }
        }

        private static void BuyBook()
        {
            string title = GetInput("Podaj tytuł ksiazki: ");
            int nrOfBooksToBuy = GetIntInput("Ile sztuk chcesz kupić? ");
            if(title=="" || nrOfBooksToBuy == -1)
            {
                Console.WriteLine("Podaj poprawne dane.");
                return;
            }
            Book book = GetBookWithTitle(title);
            if (nrOfBooksToBuy > book.NumberOfBooks)
            {
                Console.WriteLine("Niestety mamy tylko " + book.NumberOfBooks + " książek.");
            }
            else {
                book.NumberOfBooks -= nrOfBooksToBuy;
                UpdateBook(book, title);
                int cost = nrOfBooksToBuy * book.Price;
                Console.WriteLine("Kupiłeś " + nrOfBooksToBuy + " książek za "+ cost+"zł.");
            }
        }

        private static void UpdateBook(Book book, string title)
        {
            Console.WriteLine(HttpService.putPostResponse(HttpService.GET_ALL_JSON_URL + title, "PUT", jsonSerializer.Serialize(book)));
        }


        private static string GetInput(string question)
        {
            Console.WriteLine(question);
            return Console.ReadLine();
        }

        private static int GetIntInput(string question)
        {
            string stringNumber = GetInput(question);
            int number = -1;
            try
            {
                number = int.Parse(stringNumber);
            }
            catch (Exception e)
            {
                Console.WriteLine("Podaj poprawną liczbę całkowitą.");
            }
            return number;
        }
    }
}