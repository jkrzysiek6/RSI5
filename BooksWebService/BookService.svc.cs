﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BooksWebService
{
    
    public class BookService : IBookService
    {

        private static List<User> users = new List<User>()
        {
            new User("kjanowsk", "haslo", "Krzysiek", "Janowski"),
            new User("k", "h", "K", "J")
        };

        public List<User> getAllUsers()
        {
            return users;
        }



        private static List<Book> books = new List<Book>()
        {
            new Book("Harry Potter", 26, 4),
            new Book("Lsnienie", 35, 1),
        };


        public List<Book> getAll()
        {
            return books;
        }

        public List<Book> getAllJson()
        {
            return books;
        }

        public Book getById(string id)
        {
            return findByTitle(id);
        }

        public Book getBookByTitleJson(string title)
        {
            return findByTitle(title);
        }

        public string update(Book book, string title)
        {
            if (book.Title.Length == 0 || book.Price < 0 || book.NumberOfBooks < 0)
            {
                return "Bledne dane.";
            }
            Book bookToUpdate = findByTitle(book.Title);
            if (bookToUpdate == null)
            {
                return "Brak ksiazki o tytule " + book.Title + ".";
            }
            bookToUpdate.Title = book.Title;
            bookToUpdate.Price = book.Price;
            bookToUpdate.NumberOfBooks = book.NumberOfBooks;

            return "Pomyslnie zaktualizowano dane ksiazki: Tytul: " + book.Title + ", cena: " + book.Price + ", sztuk: " + book.NumberOfBooks;
        }

        private Book findByTitle(string title)
        {
            foreach (Book book in books)
            {
                if (book.Title.Equals(title))
                {
                    return book;
                }
            }
            return null;
        }

        public string addBook(Book book)
        {
            if(book.Title.Length==0 || book.Price < 0 || book.NumberOfBooks <= 0)
            {
                return "Bledne dane.";
            }

            if (findByTitle(book.Title) != null)
            {
                return "Istnieje jużz ksiazka o tytule "+book.Title+".";
            }

            books.Add(book);

            return "Pomyslnie dodano ksiazke: Tytul: " + book.Title + ", cena: " + book.Price + ", sztuk: " + book.NumberOfBooks;
        }

        public string deleteBook(string title)
        {
            if(title.Length==0)
            {
                return "Podaj tytul.";
            }
            if (books.Remove(findByTitle(title)))
            {
                return "Pomyslnie usunieto ksiazke o tytule " + title + ".";
            }
            return "Brak ksiazek o tym tytule";
        }
    }
}