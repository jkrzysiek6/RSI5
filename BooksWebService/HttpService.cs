﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace BooksWebService
{
    public class HttpService
    {
        public const string BASE_URL = "http://localhost:31692/BookService.svc/";
        public const string GET_ALL_URL = BASE_URL + "books/";
        public const string GET_ALL_JSON_URL = BASE_URL + "booksjson/";

        public static string getResponse(string uri)
        {
            HttpWebRequest request = createRequest(uri, "GET");
            return sendRequest(request);
        }

        public static string deleteResponse(string title)
        {
            HttpWebRequest request = createRequest(GET_ALL_URL + title, "DELETE");
            return sendRequest(request);
        }

        public static string putPostResponse(string uri, string method, string serializedBook)
        {
            HttpWebRequest request = createRequest(uri, method);
            addDataToRequest(request, serializedBook);
            return sendRequest(request);
        }


        private static HttpWebRequest createRequest(string uri, string method)
        {
            HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
            Console.WriteLine(request.Address);
            request.KeepAlive = false;
            request.Method = method;
            return request;
        }

        private static string sendRequest(HttpWebRequest request)
        {
            HttpWebResponse webResponse = request.GetResponse() as HttpWebResponse;
            Encoding enc = System.Text.Encoding.GetEncoding(1252);
            StreamReader loResponseStream = new StreamReader(webResponse.GetResponseStream(), enc);
            string response = loResponseStream.ReadToEnd();
            loResponseStream.Close();
            webResponse.Close();
            return response;
        }

        private static void addDataToRequest(HttpWebRequest request, string serializedBook)
        {
            Console.WriteLine(serializedBook);
            byte[] buffer = Encoding.ASCII.GetBytes(serializedBook);
            request.ContentLength = buffer.Length;
            request.ContentType = "application/json";
            Stream PostData = request.GetRequestStream();
            PostData.Write(buffer, 0, buffer.Length);
            PostData.Close();
        }
    }
}