﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BooksWebService
{
    [ServiceContract]
    public interface IBookService
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "booksjson/", Method = "POST", RequestFormat = WebMessageFormat.Json)]
        string addBook(Book book);

        [OperationContract]
        [WebGet(UriTemplate = "booksjson/", ResponseFormat = WebMessageFormat.Json)]
        List<Book> getAllJson();

        [OperationContract]
        [WebGet(UriTemplate = "booksjson/{title}", ResponseFormat = WebMessageFormat.Json)]
        Book getBookByTitleJson(string title);

        [OperationContract]
        [WebInvoke(UriTemplate = "books/{title}", Method = "DELETE")]
        string deleteBook(string title);

        [OperationContract]
        [WebInvoke(UriTemplate = "booksjson/{title}", Method = "PUT", RequestFormat = WebMessageFormat.Json)]
        string update(Book book, string title);

        [OperationContract]
        [WebGet(UriTemplate = "users/", ResponseFormat = WebMessageFormat.Json)]
        List<User> getAllUsers();
    }

    [DataContract]
    public class Book
    {
        private string title;
        private int price;
        private int numberOfBooks;

        public Book() { }
        public Book(string title, int price, int numberOfBooks)
        {
            this.title = title;
            this.price = price;
            this.numberOfBooks = numberOfBooks;
        }
        [DataMember]
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        [DataMember]
        public int Price
        {
            get { return price; }
            set { price = value; }
        }
        [DataMember]
        public int NumberOfBooks
        {
            get { return numberOfBooks; }
            set { numberOfBooks = value; }
        }
    }


    [DataContract]
    public class User
    {
        private string login, password, name, surname;
        private List<Book> usersBooks;

        public User(string login, string password, string name, string surname)
        {
            this.login = login;
            this.password = password;
            this.name = name;
            this.surname = surname;
        }

        public string Login
        {
            get { return login; }

            set { login = value; }
        }

        [DataMember]
        public string Name
        {
            get { return name; }

            set { name = value; }
        }

        [DataMember]
        public string Password
        {
            get { return password; }

            set { password = value; }
        }

        [DataMember]
        public string Surname
        {
            get { return surname; }

            set { surname = value; }
        }

        [DataMember]
        public List<Book> UsersBooks
        {
            get { return usersBooks; }

            set { usersBooks = value; }
        }
    }
}
